#include "Sequencer.h"
#include "MeasurementCollection.h"
#include <iostream>
#include <algorithm>
#include "TTree.h"
#include "TFile.h"

class MeasurementA: public Seq::MeasurementBase{
	public:
	int i;
	MeasurementA(int i_){
		i=i_;
		std::cout<<"MeasurementA::MeasurementA("<<i<<")"<<std::endl;
	}
	~MeasurementA(){
		std::cout<<"MeasurementA::~MeasurementA()"<<std::endl;
	}
	void Prepare(){
		std::cout<<"MeasurementA::Prepare() "<<i<<std::endl;
	}
	void Run(){
		std::cout<<"MeasurementA::Run() "<<++i<<std::endl;
	}
};
class MeasurementB: public Seq::MeasurementBase{
	public:
	int i;
	MeasurementB(int i_){
		i=i_;
		std::cout<<"MeasurementB::MeasurementB("<<i<<")"<<std::endl;
	}
	virtual ~MeasurementB(){
		std::cout<<"MeasurementB::~MeasurementB()"<<std::endl;
	}
	void Prepare(){
		std::cout<<"MeasurementB::Prepare()"<<i<<std::endl;
	}
	void Run(){
		std::cout<<"MeasurementB::Run()"<<++i<<std::endl;
	}
};


int main (int argc, char *argv[]){
try{
	Seq::MeasurementCollection* todo= new Seq::MeasurementCollection;
	todo->Add("job1",new MeasurementA(1));
	todo->Add("job2",new MeasurementB(1));

	Seq::Sequence<double>().LoopOver(Seq::RangeList<double>("1.1 2.2"),[&todo](Seq::Sequence<double>* this_seq){
		std::cout<<"L1: "<<this_seq->GetObservable()<<std::endl;
		todo->Loop([](Seq::MeasurementBase* job){
			Seq::Sequence<int>().LoopOver(Seq::RangeList<int>("9 10"),[&job](Seq::Sequence<int>* this_seq){
				std::cout<<"L2: "<<this_seq->GetObservable()<<std::endl;
				Seq::Sequence<int>().LoopOver(Seq::RangeSteps<int>(0,2,2),[&job](Seq::Sequence<int>* this_seq){
					std::cout<<"L3: "<<this_seq->GetObservable()<<std::endl;
					job->Run();
					return true;
				});
			return true;
			});
		});
		return true;
	});
	delete todo;
	std::cout<<"END"<<std::endl;
}
catch (std::exception& e){
	std::cerr << "Exception: " << e.what() << "\n";
}
return 0;
}
