#include "Sequencer.h"
#include <iostream>
#include <algorithm>
#include "TTree.h"
#include "TFile.h"
int main (int argc, char *argv[]){
try{
/*
	//Generation of basic lists 
	//=========================
	//Range, stepsize
	{
		std::list<double> reslist=Seq::RangeSize<double>(0,1,1);
		std::cout<<"Result: length="<<reslist.size()<<std::endl;
		for(std::list<double>::iterator it=reslist.begin();it!=reslist.end();it++){
			std::cout<<*it<<std::endl;
		}
	}

	{
		std::list<double> reslist=Seq::RangeSize<double>(0,1,.1);
		std::cout<<"Result: length="<<reslist.size()<<std::endl;
		for(std::list<double>::iterator it=reslist.begin();it!=reslist.end();it++){
			std::cout<<*it<<std::endl;
		}
	}
	//Range, number of steps
	{
		std::list<double> reslist=Seq::RangeSteps<double>(0,1,11);
		std::cout<<"Result: length="<<reslist.size()<<std::endl;
		for(std::list<double>::iterator it=reslist.begin();it!=reslist.end();it++){
			std::cout<<*it<<std::endl;
		}
	}
	//String list parsed as string-separated doubles
	{
		std::list<double> reslist=Seq::RangeList<double>("1 2 4 10 1.01");
		std::cout<<"Result: length="<<reslist.size()<<std::endl;
		for(std::list<double>::iterator it=reslist.begin();it!=reslist.end();it++){
			std::cout<<*it<<std::endl;
		}
	}

	//Loop over list using a lambda function
	std::cout<<"Foreach:"<<std::endl;
	Seq::LoopOver(Seq::RangeSteps<double>(0,1,11),[](double &item){ std::cout<<item<<std::endl; });


	//Looping over lists using the Sequence class
	//If a tree is given, save the swept parameters in a tree
	//The branches are generated automatically
	//===========================================

	std::cout<<"Sequence class:"<<std::endl;
	TTree* aTree=new TTree("ttree","Some tree");
	//Definition of the sequence class, prepare branch
	Seq::Sequence<double>* seq=new Seq::Sequence<double>(aTree,"a_double","/D");
	seq->LoopOver(Seq::RangeList<double>("1 2 4 10 1.01"),
			//
			[aTree](Seq::Sequence<double>* this_seq){
			std::cout<<"InnerFunction(): "<<item<<std::endl;
			aTree->Fill();
	});
	aTree->Print();	
	aTree->Scan();

	std::cout<<"Sequence class, no tree:"<<std::endl;
	Seq::Sequence<double>().LoopOver(Seq::RangeList<double>("1 2 4 10 1.01"),[aTree](double &item){
			std::cout<<"InnerFunction(): "<<item<<std::endl;
			aTree->Fill();
	});

*/
	//Example of using the Class for a nested sweep
	TTree* aTree2=new TTree("ttree2","Some tree");
	//Definition of the sequence class, prepare branch
	Seq::Sequence<double>(aTree2,"a_double","a_double/D").LoopOver(Seq::RangeList<double>("1 2 4 10 1.01"),
			[aTree2](Seq::Sequence<double>* this_seq){
			double item=this_seq->GetObservable();
			std::cout<<"InnerFunction(): "<<item<<std::endl;
			Seq::Sequence<int>(aTree2,"b_double","/i").LoopOver(Seq::RangeSteps<int>(0,9,10),
			[aTree2](Seq::Sequence<int>* this_seq){
				int item=this_seq->GetObservable();
				std::cout<<"InnerFunction2(): "<<item<<std::endl;
				aTree2->Fill();

				if(/*somethingbadhappened*/false) return false;
				return true; //need to return true to continue the loop

			});
			return true;
	});
	//aTree2->Print();	
	//aTree2->Scan();

	//Example of using the Class for a nested sweep, with Settling Time Requests
	//Again, a tree with two branches is created and filled with differen sequence ranges.
	//If the outer loop, the requested time is set to 100us.
	//In the inner loop, the requested time is set to 10us.
	//-->
	//As the outer loop variable advances, 100us are spent for settling. Otherwise 10us are spent

	TTree* aTree3=new TTree("ttree3","Some tree");
	TTree* aTree3b=new TTree("ttree3b","Some tree");
	std::vector<TTree*> stor_trees;
	stor_trees.push_back(aTree3);
	stor_trees.push_back(aTree3b);
	//Definition of the sequence class, prepare branch
	Seq::Sequence<double>(stor_trees,"a_double","a_double/D").LoopOver(Seq::RangeList<double>("1 2 4 10 1.01"),
			[](Seq::Sequence<double>* this_seq){

			double item=this_seq->GetObservable();
			Seq::Settling::Request(100);
			std::cout<<"InnerFunction(): "<<item<<std::endl;


			//Example using the generic Loop Method, giving the current Sequence as parameter
			Seq::Sequence<int>(this_seq->GetTrees(),"b_double","b_double/i").LoopOver(Seq::RangeSteps<int>(0,9,10),
			[](Seq::Sequence<int>* this_seq){
				int item=this_seq->GetObservable();
				Seq::Settling::Request(10);
				std::cout<<"InnerFunction2(): "<<item<<std::endl;
				Seq::Settling::Settle(); //Allow to settle
				/*
					DO THE DAQ
				*/

				this_seq->GetTree();
				if(item==9) return false;
				return true;
			});
			return true;
	});
	//aTree3->Print();
	//aTree3b->Print();
	//aTree3->Scan();
	//aTree3b->Scan();

	TFile f("test.root","recreate");
	aTree2->Write();	
	aTree3->Write();	




}
catch (std::exception& e){
	std::cerr << "Exception: " << e.what() << "\n";
}
return 0;
}
