#ifndef MEASUREMENT_COLLECTION
#define MEASUREMENT_COLLECTION

#include <string>
#include <map>

namespace Seq{
	//Interface class for a measurement
	class MeasurementBase{
		public:
			virtual ~MeasurementBase(){};
			virtual void Run()=0;
			virtual void Prepare()=0;
			virtual void Cleanup(){};

			void SetName(std::string _name){f_name=_name;};
			std::string GetName(){return f_name;};

		private:
			std::string f_name;
	};

	//Collection of measurements: Run each in Loop: lambda function Loop(lamda[](current_measurement*))
	class MeasurementCollection{
		public:
			~MeasurementCollection(){
				for (std::map<std::string,MeasurementBase*>::iterator it=collection.begin();it!=collection.end();++it){
					delete it->second;
				}
			};
			void Add(std::string name, MeasurementBase* m){collection[name]=m; m->SetName(name);}
			MeasurementBase* Get(std::string what){return collection[what];};

			void Loop(){};
			template<class UnaryFunction> bool Loop(UnaryFunction f){
				for (std::map<std::string,MeasurementBase*>::iterator it=collection.begin();it!=collection.end();++it){
					it->second->Prepare();
					f(it->second); //if (!f) return false
					it->second->Cleanup();
				}
				return true;
			}
		protected:
			std::map<std::string,MeasurementBase*> collection;
	};
};
#endif
