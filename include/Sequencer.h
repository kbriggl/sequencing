/*
 * Sequencer: Template class for sequence list generation from basic ranges or string lists
 *
 *  Created on: 23.11.2016
 *      Author: K.Briggl
 */
#ifndef SEQUENCER_H__
#define SEQUENCER_H__

#include<string>
#include<sstream>
#include<vector>
#include<list>
#include<algorithm>
#include "TTree.h"
//#define DEBUG
namespace Seq{
	template <class T> 
		std::list<T> Point(T it);
	template <class T> 
		std::list<T> RangeSize(T begin, T end, T step);
	template <class T> 
		std::list<T> RangeSteps(T begin, T end, unsigned int NSteps);
	template <class T> 
		std::list<T> RangeList(std::string separatedList);
	template <class T>
		void RangeAddEps(std::list<T>& dest, std::list<T> src,float eps=0.01); //Add src to dest, not adding points both in src and dest (using Abs(src-dst)<eps for comparison)
	template <class T>
		void RangeAddComp(std::list<T>& dest, std::list<T> src); //Add src to dest, not adding points both in src and dest (using == operator for comparison)

	template <class T, class UnaryFunction> 
		UnaryFunction LoopOver(std::list<T> li, UnaryFunction f);



	class Settling{
		public:
			//Member function: Request minimum settling time for this
			static void Request(int microseconds);
			static void Settle();
		private:
			static int m_settlingtime; 
				//Settling time requested, in microseconds
				//The maximum time requested before a Settle() call will be spent, the request resetted
	};

	template <class T> 
	class Sequence{
		public:
			Sequence(TTree* tree=NULL, std::string obs_name="seq_observable", std::string obs_leaflist="seq_observable/F");
			Sequence(std::vector<TTree*> trees, std::string obs_name="seq_observable", std::string obs_leaflist="seq_observable/F");
			~Sequence();
			virtual void AddTreeBranch(TTree* tree, std::string obs_name, std::string obs_leaflist);

			//Member function: Loop over the list. Function template has the prototype " f(Sequence<T>* == this) -> bool "
			template<class UnaryFunction> bool LoopOver(std::list<T> li, UnaryFunction f);

			//Get the Observable
			virtual T GetObservable();

			//Get the Storing tree from the vector
			virtual TTree* GetTree(int n=0);
			virtual std::vector<TTree*> GetTrees();
		protected:
			//Member function: Set up tree, add the branch
			//Members: storage of observable
			struct treestorage{
				TTree* tree;
				TBranch* branch;
			};
			std::vector<treestorage> m_storage;
			T m_observable;

	};
};

#include "Sequencer.htt"
#endif
